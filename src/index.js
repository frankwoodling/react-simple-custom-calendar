import React, { useState } from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import YearRoute from './routes/year.route.jsx';
import MonthRoute from './routes/month.route.jsx';
import WeekRoute from './routes/week.route.jsx';
import DayRoute from './routes/day.route.jsx';
import HomeRoute from './routes/home.route.jsx';
import GlobalContext from './providers/GlobalContext.context';
import { getWeekNumber } from './utils/getWeekNumber';

function App() {
  // Default to current month and year
  const date = new Date();
  const year = date.getUTCFullYear();
  const month = date.getUTCMonth();
  const day = date.getUTCDate();
  const week = getWeekNumber(year, month, day);

  // TODO: move this out to context file
  const [selectedDate, setSelectedDate] = useState({
    year,
    month,
    day,
    week,
  });

  const [config, setConfig] = useState({
    showWeekends: true,
    showSelector: true,
    showContent: true,
  });

  const globalSettings = {
    selectedDate,
    setSelectedDate,
    config,
    setConfig,
  };

  return (
    <React.StrictMode>
      <BrowserRouter>
        <GlobalContext.Provider value={globalSettings}>
          <Routes>
            <Route path="/" element={<HomeRoute />}>
              <Route path="year" element={<YearRoute />} />
              <Route path="month" element={<MonthRoute />} />
              <Route path="week" element={<WeekRoute />} />
              <Route path="day" element={<DayRoute />} />
            </Route>
          </Routes>
        </GlobalContext.Provider>
      </BrowserRouter>
    </React.StrictMode>
  );
}
ReactDOM.render(<App />, document.getElementById('root'));
