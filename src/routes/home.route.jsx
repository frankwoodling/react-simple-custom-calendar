import { Outlet } from 'react-router-dom';
import CalendarNavTimeframe from '../components/CalendarNavTimeframe/CalendarNavTimeframe.component.tsx';
import WeekendCheckbox from './../components/WeekendCheckbox/WeekendCheckbox.component.tsx';

export default function Home() {
  return (
    <main style={{ margin: 0 }}>
      <WeekendCheckbox />
      <CalendarNavTimeframe />
      <Outlet />
    </main>
  );
}
