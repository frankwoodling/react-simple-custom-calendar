import styling from '../mocks/defaultStyle.year.mock.ts';
import Year from './../containers/Year/Year.container';
import { data } from '../mocks/data.mock';
import StackedYear from '../containers/StackedYear/StackedYear.container';

export default function YearRoute() {
  return (
    <main style={{ margin: 0 }}>
      <Year defaultStyle={styling} data={data} showMonthName={true} />
      {/* <StackedYear defaultStyle={styling} data={data} showMonthName={true} /> */}
    </main>
  );
}
