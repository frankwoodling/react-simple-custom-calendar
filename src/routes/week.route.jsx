import styling from '../mocks/defaultStyle.week.mock.ts';
import Week from './../containers/Week/Week.container';
import { data } from '../mocks/data.mock';

export default function WeekRoute() {
  return (
    <main style={{ margin: 0 }}>
      <Week defaultStyle={styling} data={data} showWeekSelector={true} />
    </main>
  );
}
