import styling from '../mocks/defaultStyle.month.mock.ts';
import Month from './../containers/Month/Month.container';
import { data } from './../mocks/data.mock';

export default function MonthRoute() {
  return (
    <main style={{ margin: 0 }}>
      <Month
        defaultStyle={styling}
        data={data}
        showMonthYearSelector={true}
        showWeekdayHeader={true}
        type={'month'}
      />
    </main>
  );
}
