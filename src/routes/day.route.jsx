import styling from '../mocks/defaultStyle.day.mock.ts';
import { data } from '../mocks/data.mock';
import Day from './../containers/Day/Day.container';

export default function DayRoute() {
  return (
    <main style={{ margin: 0 }}>
      <Day defaultStyle={styling} data={data} showDaySelector={true} />
    </main>
  );
}
