import React from 'react';
import { NavLink } from 'react-router-dom';
import './CalendarNavTimeframe.style.css';

const CalendarNavTimeframe = () => {
  return (
    <div className="nav-wrapper">
      <NavLink
        to="/day"
        className={({ isActive }) =>
          `${'nav-link'} ${isActive ? 'nav-link-current' : undefined}`
        }
      >
        Day
      </NavLink>
      <NavLink
        to="/week"
        className={({ isActive }) =>
          `${'nav-link'} ${isActive ? 'nav-link-current' : undefined}`
        }
      >
        Week
      </NavLink>
      <NavLink
        to="/month"
        className={({ isActive }) =>
          `${'nav-link'} ${isActive ? 'nav-link-current' : undefined}`
        }
      >
        Month
      </NavLink>
      <NavLink
        to="/year"
        className={({ isActive }) =>
          `${'nav-link'} ${isActive ? 'nav-link-current' : undefined}`
        }
      >
        Year
      </NavLink>
    </div>
  );
};

export default CalendarNavTimeframe;
