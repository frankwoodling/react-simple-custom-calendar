import React, { useContext } from 'react';
import GlobalContext from '../../providers/GlobalContext.context';
import './WeekendCheckbox.style.css';

const WeekendCheckbox = () => {
  const ctx = useContext(GlobalContext);
  const { showWeekends } = ctx.config;

  const handleChange = () => {
    ctx.setConfig({
      ...ctx.config,
      showWeekends: !ctx.config.showWeekends,
    });
  };

  return (
    <div className="checkbox-wrapper">
      <label style={{ color: 'white' }}>
        <input type="checkbox" checked={showWeekends} onChange={handleChange} />
        Show Weekends
      </label>
    </div>
  );
};

export default WeekendCheckbox;
