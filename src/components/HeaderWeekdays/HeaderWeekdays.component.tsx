import React, { useContext } from 'react';
import './HeaderWeekdays.style.css';
import GlobalContext from '../../providers/GlobalContext.context';

const HeaderWeekdays = () => {
  const ctx = useContext(GlobalContext);
  const { showWeekends } = ctx.config;

  const weekdayAbbrev = ['Sun', 'Mon', 'Tue', 'Wed', 'Thur', 'Fri', 'Sat'];
  return (
    <div
      className="weekday-title-wrapper"
      style={{ gridTemplateColumns: `repeat(${showWeekends ? 7 : 5}, 1fr)` }}
    >
      {weekdayAbbrev.map((day) => {
        // Omits Sat and Sun labels if weekends aren't shown
        if (showWeekends || (!showWeekends && day !== 'Sun' && day !== 'Sat')) {
          return (
            <h2 key={day} className="weekday-text">
              {day}
            </h2>
          );
        }
      })}
    </div>
  );
};

export default HeaderWeekdays;
