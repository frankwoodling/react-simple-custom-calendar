import React, { useContext } from 'react';
import GlobalContext from '../../providers/GlobalContext.context';
import './YearSelector.style.css';

const YearSelector = () => {
  const ctx = useContext(GlobalContext);
  const { year } = ctx.selectedDate;

  return (
    <div className="date-selector-wrapper">
      <button
        className="date-selector-button"
        onClick={() => {
          ctx.setSelectedDate({
            ...ctx.selectedDate,
            year: year - 1,
          });
        }}
      >
        <svg
          xmlns="http://www.w3.org/2000/svg"
          height="24px"
          viewBox="0 0 24 24"
          width="24px"
          fill="rgba(255, 255, 255, 0.6)"
        >
          <path d="M0 0h24v24H0z" fill="none" />
          <path d="M20 11H7.83l5.59-5.59L12 4l-8 8 8 8 1.41-1.41L7.83 13H20v-2z" />
        </svg>
      </button>

      <div className="date-selector-text-wrapper">
        <h2 className="date-selector-year">{year}</h2>
      </div>

      <button
        className="date-selector-button"
        onClick={() => {
          ctx.setSelectedDate({
            ...ctx.selectedDate,
            year: year + 1,
          });
        }}
      >
        <svg
          xmlns="http://www.w3.org/2000/svg"
          height="24px"
          viewBox="0 0 24 24"
          width="24px"
          fill="rgba(255, 255, 255, 0.6)"
        >
          <path d="M0 0h24v24H0z" fill="none" />
          <path d="M12 4l-1.41 1.41L16.17 11H4v2h12.17l-5.58 5.59L12 20l8-8z" />
        </svg>
      </button>
    </div>
  );
};

export default YearSelector;
