import React, { useContext } from 'react';
import { useNavigate } from 'react-router-dom';
import GlobalContext from '../../providers/GlobalContext.context';
import './DayCard.style.css';

type Card = {
  title?: string;
  date?: Date;
  content?: any;
  cardStyle?: CardStyle;
  defaultStyle?: CardStyle;
  style?: {};
};

export type CardStyle = {
  card?: {};
  cardTitleWrapper?: {};
  cardTitle?: {};
  cardContentWrapper?: {};
  cardContent?: {};
};

const DayCard = ({
  title = '',
  date,
  content = '',
  cardStyle,
  defaultStyle,
  style,
}: Card) => {
  const ctx = useContext(GlobalContext);
  const navigate = useNavigate();

  const setDateOnClick = (e) => {
    const clickedDate = new Date(e.currentTarget.getAttribute('data-value'));
    const clickedYear = clickedDate.getUTCFullYear();
    const clickedMonth = clickedDate.getUTCMonth();
    const clickedDay = clickedDate.getUTCDate();

    ctx.setSelectedDate({
      ...ctx.selectedDate,
      year: clickedYear,
      month: clickedMonth,
      day: clickedDay,
    });

    navigate('/day');
  };

  return (
    <div
      className="card"
      style={{ ...style, ...defaultStyle?.card, ...cardStyle?.card }}
      data-value={date}
      onClick={(e) => setDateOnClick(e)}
    >
      <div
        className="card-title-wrapper"
        style={{
          ...defaultStyle?.cardTitleWrapper,
          ...cardStyle?.cardTitleWrapper,
        }}
      >
        <h2
          className="card-title"
          style={{ ...defaultStyle?.cardTitle, ...cardStyle?.cardTitle }}
        >
          {title}
        </h2>
      </div>
      <div
        className="card-content-wrapper"
        style={{
          ...defaultStyle?.cardContentWrapper,
          ...cardStyle?.cardContentWrapper,
        }}
      >
        {content}
      </div>
    </div>
  );
};

export default DayCard;
