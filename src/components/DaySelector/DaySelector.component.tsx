import React, { useContext } from 'react';
import GlobalContext from '../../providers/GlobalContext.context';
import './DaySelector.style.css';
import { lookupMonth } from '../../utils/lookupMonth';
import { getWeekNumber } from '../../utils/getWeekNumber';
import { addDaysToDate } from '../../utils/addDaysToDate';

const DaySelector = () => {
  const ctx = useContext(GlobalContext);
  const { day, month, year } = ctx.selectedDate;

  return (
    <div className="date-selector-wrapper">
      <button
        className="date-selector-button"
        onClick={() => {
          // Convert to date, subtract 7 days, and convert back to day, month, year for context
          let newDate = addDaysToDate(year, month, day, -1);

          const newWeekNumber = getWeekNumber(
            newDate.getUTCFullYear(),
            newDate.getUTCMonth(),
            newDate.getUTCDate()
          );

          ctx.setSelectedDate({
            ...ctx.selectedDate,
            day: newDate.getUTCDate(),
            week: newWeekNumber,
            month: newDate.getUTCMonth(),
            year: newDate.getUTCFullYear(),
          });
        }}
      >
        <svg
          xmlns="http://www.w3.org/2000/svg"
          height="24px"
          viewBox="0 0 24 24"
          width="24px"
          fill="rgba(255, 255, 255, 0.6)"
        >
          <path d="M0 0h24v24H0z" fill="none" />
          <path d="M20 11H7.83l5.59-5.59L12 4l-8 8 8 8 1.41-1.41L7.83 13H20v-2z" />
        </svg>
      </button>

      <div className="date-selector-text-wrapper">
        <h2 className="date-selector-text">
          {lookupMonth(month) + ' ' + day + ', ' + year}
        </h2>
      </div>

      <button
        className="date-selector-button"
        onClick={() => {
          // Convert to date, add 7 days, and convert back to day, month, year for context
          let newDate = addDaysToDate(year, month, day, 1);

          const newWeekNumber = getWeekNumber(
            newDate.getUTCFullYear(),
            newDate.getUTCMonth(),
            newDate.getUTCDate()
          );

          ctx.setSelectedDate({
            ...ctx.selectedDate,
            day: newDate.getUTCDate(),
            week: newWeekNumber,
            month: newDate.getUTCMonth(),
            year: newDate.getUTCFullYear(),
          });
        }}
      >
        <svg
          xmlns="http://www.w3.org/2000/svg"
          height="24px"
          viewBox="0 0 24 24"
          width="24px"
          fill="rgba(255, 255, 255, 0.6)"
        >
          <path d="M0 0h24v24H0z" fill="none" />
          <path d="M12 4l-1.41 1.41L16.17 11H4v2h12.17l-5.58 5.59L12 20l8-8z" />
        </svg>
      </button>
    </div>
  );
};

export default DaySelector;
