import React, { useContext } from 'react';
import DayCard from '../../components/DayCard/DayCard.component.tsx';
import HeaderWeekdays from '../../components/HeaderWeekdays/HeaderWeekdays.component.tsx';
import MonthYearSelector from '../../components/MonthYearSelector/MonthYearSelector.component.tsx';
import GlobalContext from '../../providers/GlobalContext.context';
import { getDayRangeFromMonthYear } from '../../utils/getDayRangeFromMonthYear';
import { getWeekday } from '../../utils/getWeekday';
import { dateLookup } from '../../utils/dateLookup';
import './Month.style.css';
// TODO: Make absolute imports throughout
// https://hackernoon.com/react-pro-tip-use-absolute-imports-for-better-readability-and-easy-refactoring-2ad5c7f2f957

const Month = ({
  defaultStyle,
  data,
  showMonthYearSelector = false,
  showWeekdayHeader = false,
  yearExplicit,
  monthExplicit,
  type = 'month',
}) => {
  const ctx = useContext(GlobalContext);
  const { year, month } = ctx.selectedDate;
  const { showWeekends } = ctx.config;

  const dateRange =
    monthExplicit >= 0 && yearExplicit && type === 'year'
      ? getDayRangeFromMonthYear(monthExplicit, yearExplicit, showWeekends)
      : getDayRangeFromMonthYear(month, year, showWeekends);

  const daysToSkip = showWeekends
    ? getWeekday(dateRange[0])
    : getWeekday(dateRange[0]) - 1;

  const nGridRows = showWeekends
    ? Math.ceil((dateRange.length + daysToSkip) / 7)
    : Math.ceil((dateRange.length + daysToSkip) / 5);
  const [, ...rest] = dateRange;

  const checkMatch = (day) => {
    return dateLookup(
      day.getUTCFullYear(),
      day.getUTCMonth(),
      day.getUTCDate(),
      data
    );
  };

  return (
    <>
      {showMonthYearSelector && <MonthYearSelector />}
      {showWeekdayHeader && <HeaderWeekdays />}

      <div
        className="month-wrapper"
        style={{
          // TODO: Day cards will be different sizes for 5 and 6 week months.  Setting gridTemplateRows to 6 fixes the sizing but adds in a gap at the bottom.
          gridTemplateRows: `repeat(${nGridRows}, 1fr)`,
          gridTemplateColumns: `repeat(${showWeekends ? 7 : 5}, 1fr)`,
        }}
      >
        <DayCard
          key={dateRange[0]}
          date={dateRange[0]}
          title={dateRange[0].getUTCDate()}
          content={checkMatch(dateRange[0])?.content ?? undefined}
          defaultStyle={defaultStyle}
          cardStyle={checkMatch(dateRange[0])?.cardStyle ?? undefined}
          style={{ gridColumn: daysToSkip + 1 }}
          className="card-first"
        />

        {rest.map((day) => {
          const match = checkMatch(day);

          return (
            <DayCard
              key={day}
              date={day}
              title={day.getUTCDate()}
              content={match ? match?.content : undefined}
              cardStyle={match ? match?.cardStyle : undefined}
              defaultStyle={defaultStyle}
            />
          );
        })}
      </div>
    </>
  );
};

export default Month;
