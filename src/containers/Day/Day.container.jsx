import React, { useContext } from 'react';
import DayCard from '../../components/DayCard/DayCard.component.tsx';
import DaySelector from '../../components/DaySelector/DaySelector.component.tsx';
import GlobalContext from '../../providers/GlobalContext.context';
import { dateLookup } from '../../utils/dateLookup';
import './Day.style.css';
import { lookupMonth } from '../../utils/lookupMonth';

const Day = ({
  defaultStyle,
  data,
  showDaySelector = false,
  showDateTitle = false,
}) => {
  const ctx = useContext(GlobalContext);
  const { day, month, year } = ctx.selectedDate;

  const checkMatch = (day) => {
    return dateLookup(
      day.getUTCFullYear(),
      day.getUTCMonth(),
      day.getUTCDate(),
      data
    );
  };
  const date = new Date(year, month, day);
  const match = checkMatch(date);

  return (
    <>
      {showDaySelector && <DaySelector />}
      {showDateTitle && (
        <h1 className="date-text">
          {lookupMonth(month) + ' ' + day + ', ' + year}
        </h1>
      )}
      <div className="day-wrapper">
        <DayCard
          key={day}
          date={date}
          title={day}
          content={match ? match?.content : undefined}
          cardStyle={match ? match?.cardStyle : undefined}
          defaultStyle={defaultStyle}
        />
      </div>
    </>
  );
};

export default Day;
