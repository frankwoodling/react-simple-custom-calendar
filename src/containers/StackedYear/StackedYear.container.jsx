import React, { useContext } from 'react';
import DayCard from '../../components/DayCard/DayCard.component.tsx';
import YearSelector from '../../components/YearSelector/YearSelector.component.tsx';
import GlobalContext from '../../providers/GlobalContext.context';
import { dateLookup } from '../../utils/dateLookup';
import { getWeekday } from '../../utils/getWeekday';
import { filterWeekendDates } from '../../utils/filterWeekendDates';
import { getDaysFromDateRange } from '../../utils/getDaysFromDateRange';
import './StackedYear.style.css';

const StackedYear = ({ defaultStyle, data, showMonthNames = false }) => {
  console.log('defaultStyle', defaultStyle);
  // TODO: Add explicit year prop
  const ctx = useContext(GlobalContext);
  const { year } = ctx.selectedDate;
  const { showWeekends } = ctx.config;

  // const dateArray = getDaysFromDateRange(`${year}-01-02`, `${year}-12-31`);
  const dateArray = showWeekends
    ? getDaysFromDateRange(`${year}-01-01`, `${year}-12-31`)
    : filterWeekendDates(
        getDaysFromDateRange(`${year}-01-01`, `${year}-12-31`)
      );
  // const dateArrayWeekdays = filterWeekendDates(dateArray);

  const daysToSkip = showWeekends
    ? getWeekday(dateArray[0])
    : getWeekday(dateArray[0]) - 1;

  const [, ...rest] = dateArray;

  const checkMatch = (day) => {
    return dateLookup(
      day.getUTCFullYear(),
      day.getUTCMonth(),
      day.getUTCDate(),
      data
    );
  };

  return (
    <div className="stacked-year-wrapper">
      <YearSelector />

      <div
        className="stacked-year-month-wrapper"
        style={{
          // TODO: Day cards will be different sizes for 5 and 6 week months.  Setting gridTemplateRows to 6 fixes the sizing but adds in a gap at the bottom.
          gridTemplateColumns: `repeat(${showWeekends ? 7 : 5}, 1fr)`,
        }}
      >
        <DayCard
          key={dateArray[0]}
          date={dateArray[0]}
          title={dateArray[0].getUTCDate()}
          content={checkMatch(dateArray[0])?.content ?? undefined}
          defaultStyle={defaultStyle}
          cardStyle={checkMatch(dateArray[0])?.cardStyle ?? undefined}
          style={{ gridColumn: daysToSkip + 1 }}
          className="card-first"
        />
        {rest.map((day) => {
          const match = checkMatch(day);

          return (
            <DayCard
              key={day}
              date={day}
              title={day.getUTCDate()}
              content={match ? match?.content : undefined}
              cardStyle={match ? match?.cardStyle : undefined}
              defaultStyle={defaultStyle}
            />
          );
        })}
      </div>
    </div>
  );
};

export default StackedYear;
