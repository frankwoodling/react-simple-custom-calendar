import React, { useContext } from 'react';
import { useNavigate } from 'react-router-dom';
import YearSelector from '../../components/YearSelector/YearSelector.component.tsx';
import GlobalContext from '../../providers/GlobalContext.context';
import { lookupMonth } from '../../utils/lookupMonth';
import Month from '../Month/Month.container';
import './Year.style.css';

const Year = ({ defaultStyle, data, showMonthName = false }) => {
  // TODO: Add explicit year prop
  const ctx = useContext(GlobalContext);
  const navigate = useNavigate();
  const { year, day } = ctx.selectedDate;

  const setDateOnClick = (e) => {
    const clickedDate = new Date(e.currentTarget.getAttribute('data-value'));
    const clickedYear = clickedDate.getUTCFullYear();
    const clickedMonth = clickedDate.getUTCMonth();
    const clickedDay = clickedDate.getUTCDate();

    ctx.setSelectedDate({
      ...ctx.selectedDate,
      year: clickedYear,
      month: clickedMonth,
      day: clickedDay,
    });

    navigate('/month');
  };

  return (
    <div className="year-wrapper">
      <YearSelector />
      <div className="months-wrapper">
        {[...Array(12)].map((e, i) => {
          let month = lookupMonth(i);
          const date = new Date(year, i, day);
          return (
            <div key={e + ',' + i}>
              {showMonthName && (
                <h1
                  className="month-text"
                  data-value={date}
                  onClick={(e) => setDateOnClick(e)}
                >
                  <span className="month-text-span">{month}</span>
                </h1>
              )}
              <Month
                defaultStyle={defaultStyle}
                data={data}
                monthExplicit={i}
                yearExplicit={year}
                type="year"
              />
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default Year;
