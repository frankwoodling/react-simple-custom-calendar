import React, { useContext } from 'react';
import DayCard from '../../components/DayCard/DayCard.component.tsx';
import HeaderWeekdays from '../../components/HeaderWeekdays/HeaderWeekdays.component.tsx';
import WeekSelector from '../../components/WeekSelector/WeekSelector.component.tsx';
import GlobalContext from '../../providers/GlobalContext.context';
import { getDaysOfWeek } from '../../utils/getDaysOfWeek';
import { dateLookup } from '../../utils/dateLookup';

import './Week.style.css';
// TODO: Make absolute imports throughout
// https://hackernoon.com/react-pro-tip-use-absolute-imports-for-better-readability-and-easy-refactoring-2ad5c7f2f957

const Week = ({ defaultStyle, data, showWeekSelector = false }) => {
  const ctx = useContext(GlobalContext);
  const { day, month, year } = ctx.selectedDate;
  const { showWeekends } = ctx.config;
  let days = getDaysOfWeek(year, month, day);

  // Remove Sat and Sun from date object
  if (!showWeekends) days = days.slice(1, -1);

  return (
    <>
      {showWeekSelector && <WeekSelector />}
      <HeaderWeekdays />

      <div
        className="week-wrapper"
        style={{ gridTemplateColumns: `repeat(${showWeekends ? 7 : 5}, 1fr)` }}
      >
        {days.map((day) => {
          let currentDay = day.getDate();
          const match = dateLookup(
            day.getUTCFullYear(),
            day.getUTCMonth(),
            day.getUTCDate(),
            data
          );

          return (
            <DayCard
              key={day}
              date={day}
              title={currentDay}
              content={match ? match?.content : undefined}
              cardStyle={match ? match?.cardStyle : undefined}
              defaultStyle={defaultStyle}
            />
          );
        })}
      </div>
    </>
  );
};

export default Week;
