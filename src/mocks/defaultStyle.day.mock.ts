import { CardStyle } from '../components/DayCard/DayCard.component';

const defaultStylingMonth: CardStyle = {
  card: { backgroundColor: '#4F4F4F' },
  cardTitleWrapper: { outline: 'none' },
  cardTitle: { color: 'rgba(255, 255, 255, 0.6)', fontSize: '4rem' },
  cardContentWrapper: { fontSize: '5rem' },
};

export default defaultStylingMonth;
