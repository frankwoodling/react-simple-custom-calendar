export const data = [
  {
    date: '2022-01-03',
    content: (
      <>
        <h4 className="card-content">Value</h4>
        <h4 className="card-content">8.73%</h4>
      </>
    ),
    cardStyle: {
      card: { backgroundColor: '#7ABD91' },
      cardTitleWrapper: {},
      cardTitle: {},
      cardContentWrapper: { color: 'rgba(255, 255, 255, .87)' },
    },
  },
  {
    date: '2022-01-04',
    content: (
      <>
        <h4 className="card-content">Value</h4>
        <h4 className="card-content">-5.51%</h4>
      </>
    ),
    cardStyle: {
      card: { backgroundColor: '#FFB3A5' },
      cardTitleWrapper: {},
      cardTitle: {},
      cardContentWrapper: { color: 'rgba(255, 255, 255, .87)' },
    },
  },
  {
    date: '2022-01-05',
    content: (
      <>
        <h4 className="card-content">Value</h4>
        <h4 className="card-content">14.23%</h4>
      </>
    ),
    cardStyle: {
      card: { backgroundColor: '#7ABD91' },
      cardTitleWrapper: {},
      cardTitle: {},
      cardContentWrapper: { color: 'rgba(255, 255, 255, .87)' },
    },
  },
  {
    date: '2022-01-06',
    content: (
      <>
        <h4 className="card-content">Value</h4>
        <h4 className="card-content">10.49%</h4>
      </>
    ),
    cardStyle: {
      card: { backgroundColor: '#7ABD91' },
      cardTitleWrapper: {},
      cardTitle: {},
      cardContentWrapper: { color: 'rgba(255, 255, 255, .87)' },
    },
  },
  {
    date: '2022-01-07',
    content: (
      <>
        <h4 className="card-content">Value</h4>
        <h4 className="card-content">6.41%</h4>
      </>
    ),
    cardStyle: {
      card: { backgroundColor: '#7ABD91' },
      cardTitleWrapper: {},
      cardTitle: {},
      cardContentWrapper: { color: 'rgba(255, 255, 255, .87)' },
    },
  },
  {
    date: '2022-01-10',
    content: (
      <>
        <h4 className="card-content">Value</h4>
        <h4 className="card-content">-30.18%</h4>
      </>
    ),
    cardStyle: {
      card: { backgroundColor: '#FFB3A5' },
      cardTitleWrapper: {},
      cardTitle: {},
      cardContentWrapper: { color: 'rgba(255, 255, 255, .87)' },
    },
  },
  {
    date: '2022-01-11',
    content: (
      <>
        <h4 className="card-content">Value</h4>
        <h4 className="card-content">6.17%</h4>
      </>
    ),
    cardStyle: {
      card: { backgroundColor: '#7ABD91' },
      cardTitleWrapper: {},
      cardTitle: {},
      cardContentWrapper: { color: 'rgba(255, 255, 255, .87)' },
    },
  },
  {
    date: '2022-01-12',
    content: (
      <>
        <h4 className="card-content">Value</h4>
        <h4 className="card-content">16.04%</h4>
      </>
    ),
    cardStyle: {
      card: { backgroundColor: '#7ABD91' },
      cardTitleWrapper: {},
      cardTitle: {},
      cardContentWrapper: { color: 'rgba(255, 255, 255, .87)' },
    },
  },
  {
    date: '2022-01-13',
    content: (
      <>
        <h4 className="card-content">Value</h4>
        <h4 className="card-content">4.99%</h4>
      </>
    ),
    cardStyle: {
      card: { backgroundColor: '#7ABD91' },
      cardTitleWrapper: {},
      cardTitle: {},
      cardContentWrapper: { color: 'rgba(255, 255, 255, .87)' },
    },
  },
  {
    date: '2022-01-14',
    content: (
      <>
        <h4 className="card-content">Value</h4>
        <h4 className="card-content">7.42%</h4>
      </>
    ),
    cardStyle: {
      card: { backgroundColor: '#7ABD91' },
      cardTitleWrapper: {},
      cardTitle: {},
      cardContentWrapper: { color: 'rgba(255, 255, 255, .87)' },
    },
  },
  {
    date: '2022-01-18',
    content: (
      <>
        <h4 className="card-content">Value</h4>
        <h4 className="card-content">-1.24%</h4>
      </>
    ),
    cardStyle: {
      card: { backgroundColor: '#FFB3A5' },
      cardTitleWrapper: {},
      cardTitle: {},
      cardContentWrapper: { color: 'rgba(255, 255, 255, .87)' },
    },
  },
  {
    date: '2022-01-19',
    content: (
      <>
        <h4 className="card-content">Value</h4>
        <h4 className="card-content">-10.87%</h4>
      </>
    ),
    cardStyle: {
      card: { backgroundColor: '#FFB3A5' },
      cardTitleWrapper: {},
      cardTitle: {},
      cardContentWrapper: { color: 'rgba(255, 255, 255, .87)' },
    },
  },
  {
    date: '2022-01-20',
    content: (
      <>
        <h4 className="card-content">Value</h4>
        <h4 className="card-content">2.21%</h4>
      </>
    ),
    cardStyle: {
      card: { backgroundColor: '#7ABD91' },
      cardTitleWrapper: {},
      cardTitle: {},
      cardContentWrapper: { color: 'rgba(255, 255, 255, .87)' },
    },
  },
  {
    date: '2022-01-21',
    content: (
      <>
        <h4 className="card-content">Value</h4>
        <h4 className="card-content">-10.84%</h4>
      </>
    ),
    cardStyle: {
      card: { backgroundColor: '#FFB3A5' },
      cardTitleWrapper: {},
      cardTitle: {},
      cardContentWrapper: { color: 'rgba(255, 255, 255, .87)' },
    },
  },
  {
    date: '2022-01-24',
    content: (
      <>
        <h4 className="card-content">Value</h4>
        <h4 className="card-content">0.84%</h4>
      </>
    ),
    cardStyle: {
      card: { backgroundColor: '#7ABD91' },
      cardTitleWrapper: {},
      cardTitle: {},
      cardContentWrapper: { color: 'rgba(255, 255, 255, .87)' },
    },
  },
  {
    date: '2022-01-25',
    content: (
      <>
        <h4 className="card-content">Value</h4>
        <h4 className="card-content">1.47%</h4>
      </>
    ),
    cardStyle: {
      card: { backgroundColor: '#7ABD91' },
      cardTitleWrapper: {},
      cardTitle: {},
      cardContentWrapper: { color: 'rgba(255, 255, 255, .87)' },
    },
  },
  {
    date: '2022-01-26',
    content: (
      <>
        <h4 className="card-content">Value</h4>
        <h4 className="card-content">0.40%</h4>
      </>
    ),
    cardStyle: {
      card: { backgroundColor: '#7ABD91' },
      cardTitleWrapper: {},
      cardTitle: {},
      cardContentWrapper: { color: 'rgba(255, 255, 255, .87)' },
    },
  },
  {
    date: '2022-01-27',
    content: (
      <>
        <h4 className="card-content">Value</h4>
        <h4 className="card-content">-4.07%</h4>
      </>
    ),
    cardStyle: {
      card: { backgroundColor: '#FFB3A5' },
      cardTitleWrapper: {},
      cardTitle: {},
      cardContentWrapper: { color: 'rgba(255, 255, 255, .87)' },
    },
  },
  {
    date: '2022-01-28',
    content: (
      <>
        <h4 className="card-content">Value</h4>
        <h4 className="card-content">1.55%</h4>
      </>
    ),
    cardStyle: {
      card: { backgroundColor: '#7ABD91' },
      cardTitleWrapper: {},
      cardTitle: {},
      cardContentWrapper: { color: 'rgba(255, 255, 255, .87)' },
    },
  },
  {
    date: '2022-01-31',
    content: (
      <>
        <h4 className="card-content">Value</h4>
        <h4 className="card-content">2.29%</h4>
      </>
    ),
    cardStyle: {
      card: { backgroundColor: '#7ABD91' },
      cardTitleWrapper: {},
      cardTitle: {},
      cardContentWrapper: { color: 'rgba(255, 255, 255, .87)' },
    },
  },
  {
    date: '2022-02-01',
    content: (
      <>
        <h4 className="card-content">Value</h4>
        <h4 className="card-content">5.89%</h4>
      </>
    ),
    cardStyle: {
      card: { backgroundColor: '#7ABD91' },
      cardTitleWrapper: {},
      cardTitle: {},
      cardContentWrapper: { color: 'rgba(255, 255, 255, .87)' },
    },
  },
];

// Green #7ABD91
// Red #FFB3A5
