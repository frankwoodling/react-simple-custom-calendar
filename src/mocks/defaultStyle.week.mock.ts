import { CardStyle } from '../components/DayCard/DayCard.component';

const defaultStylingWeek: CardStyle = {
  card: { backgroundColor: '#4F4F4F', minHeight: '5rem', minWidth: '5rem' },
  cardTitleWrapper: { outline: 'none', height: '50%' },
  cardTitle: {
    color: 'rgba(255, 255, 255, .6)',
    textAlign: 'center',
    verticalAlign: 'middle',
    fontSize: '1.2em',
  },
  cardContentWrapper: { fontSize: '1.2rem' },
};

export default defaultStylingWeek;
