import { CardStyle } from '../components/DayCard/DayCard.component';

const defaultStylingYear: CardStyle = {
  card: { backgroundColor: '#4F4F4F' },
  cardTitleWrapper: { outline: 'none', height: '100%' },
  cardTitle: {
    color: 'rgba(255, 255, 255, .6)',
    textAlign: 'center',
    verticalAlign: 'middle',
    fontSize: '3rem',
  },
  cardContentWrapper: { fontSize: 0, display: 'none' },
};

export default defaultStylingYear;
