import { CardStyle } from '../components/DayCard/DayCard.component';

const defaultStylingStackedYear: CardStyle = {
  card: { backgroundColor: '#4F4F4F', width: '55px', height: '55px' },
  cardTitleWrapper: { outline: 'none', height: '100%' },
  cardTitle: {
    color: 'rgba(255, 255, 255, .6)',
    textAlign: 'center',
    verticalAlign: 'middle',
    fontSize: '1rem',
  },
  cardContentWrapper: { fontSize: 0, display: 'none' },
};

export default defaultStylingStackedYear;
