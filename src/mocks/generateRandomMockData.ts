const fs = require('fs');

const startDate = new Date('2019-02-01');
const endDate = new Date();
let currentMoney = 10000;
const greenDayBackground = '#7ABD91';
const redDayBackground = '#FFB3A5';
const evenDayBackground = '#4F4F4F';
const gainRange = [-2, 2.5];

let data = [];
let backgroundColor;

for (let day = startDate; day <= endDate; day.setDate(day.getDate() + 1)) {
  let dailyPercent = getRandomFloat(gainRange[0], gainRange[1]);
  let dailyDollarAmount = Math.round(currentMoney * (dailyPercent / 100));
  currentMoney += dailyDollarAmount;

  let contentElement = `<div>
    <h4 className="card-content">${dailyDollarAmount}</h4>
    <h4 className="card-content">${dailyPercent}%</h4>
  </div>`;

  if (dailyDollarAmount > 0) {
    backgroundColor = greenDayBackground;
  } else if (dailyDollarAmount < 0) {
    backgroundColor = redDayBackground;
  } else if (dailyDollarAmount === 0) {
    backgroundColor = evenDayBackground;
  }

  let dayObject = {
    date: `${day.toISOString().substring(0, 10)}`,
    content: contentElement,
    cardStyle: {
      card: { backgroundColor: `${backgroundColor}` },
      cardTitleWrapper: {},
      cardTitle: {},
      cardContentWrapper: { color: 'rgba(255, 255, 255, .87)' },
    },
  };

  data = [...data, dayObject];
}

// Write data object array to a new file
const fileName = './src/mocks/data.mock.js';
const declareConst = 'export const data = ';
fs.writeFileSync(fileName, declareConst, 'utf-8');
fs.appendFile(fileName, JSON.stringify(data), () => {});

function getRandomFloat(min, max, decimalPlaces = 2) {
  const randomFloat = Math.random() * (max - min) + min;

  return randomFloat.toFixed(decimalPlaces);
}

// console.log(JSON.parse(JSON.stringify(contentElement)));
// JSON.parse needs to happen where content is consumed
