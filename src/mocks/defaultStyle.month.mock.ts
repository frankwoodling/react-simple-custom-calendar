import { CardStyle } from '../components/DayCard/DayCard.component';

const defaultStylingMonth: CardStyle = {
  card: { backgroundColor: '#4F4F4F' },
  cardTitleWrapper: {},
  cardTitle: { color: 'rgba(255, 255, 255, 0.6)' },
  cardContentWrapper: { fontSize: 20 },
};

export default defaultStylingMonth;
