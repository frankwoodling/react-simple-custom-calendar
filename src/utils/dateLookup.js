/**
 * Takes a date object or string and converts it to ISO format.  Then
 * checks a data object for date matches and returns object if there is a match.
 * @param {*} date
 * @param {*} data
 * @returns
 */

export const dateLookup = (year, month, day, data) => {
  const date = new Date(year, month, day);
  const isoString = date.toISOString().substring(0, 10);

  return data.find(({ date }) => date === isoString);
};
