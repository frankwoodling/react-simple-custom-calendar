/**
 * Given a current day return an array of date objects for the entire week.
 *
 * Example: getDaysOfWeek(2022, 1, 29);
 *
 * @param {*} year
 * @param {*} month // 0-11
 * @param {*} day
 *
 */

const getDaysOfWeek = (year, month, day) => {
  const selectedDate = new Date(year, month, day);
  const dayOfWeek = selectedDate.getDay();
  const startDate = new Date(
    selectedDate.getTime() - dayOfWeek * 86400000 // Convert milliseconds
  );

  let week = [];
  for (let i = 0; i < 7; i++) {
    week.push(new Date(startDate.getTime() + i * 86400000));
  }

  return week;
};

module.exports = { getDaysOfWeek };
