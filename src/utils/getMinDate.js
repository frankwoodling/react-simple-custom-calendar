/**
 * Given an array of date object types return the minimum date.
 * @param {*} dateArray
 * @returns
 */
const getMibDate = (dateArray) => {
  return new Date(Math.min(...dateArray));
};

module.exports = { getMinDate };
