const filterWeekendDates = (dateObjectArray) => {
  let filteredArray = dateObjectArray.filter(
    (day) => day.getDay() !== 0 && day.getDay() !== 6
  );

  return filteredArray;
};

module.exports = { filterWeekendDates };
