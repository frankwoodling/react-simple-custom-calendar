/**
 * Give a date string for the start and end dates and
 * return an array of date objects between the two.
 *
 * Example: getDaysFromDateRange('2021-01-01', '2021-01-31');
 *
 * @param {*} startDate
 * @param {*} endDate
 * @returns
 */
const getDaysFromDateRange = (startDate, endDate) => {
  const end = new Date(endDate);
  let dateRange = [];
  let currentDate = new Date(`${startDate}T12:00:00`);

  while (currentDate < end) {
    dateRange = [...dateRange, new Date(currentDate)];
    currentDate.setDate(currentDate.getDate() + 1);
  }
  return [...dateRange, new Date(currentDate)];
};

module.exports = { getDaysFromDateRange };
