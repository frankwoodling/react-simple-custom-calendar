/**
 * Take date object and return the day of the week as an integer.
 * 0 Sunday
 * 1 Monday
 * 2 Tuesday
 * 3 Wednesday
 * 4 Thursday
 * 5 Friday
 * 6 Saturday
 *
 * Example: getWeekday(new Date('2021-01-09'))
 *
 * @param {*} date
 * @returns
 */
const getWeekday = (date) => {
  return date.getDay();
};

module.exports = { getWeekday };
