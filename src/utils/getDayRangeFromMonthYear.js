/**
 * Give a month and year and return a date object array of all days in the month.
 *
 * Example: getDayRangeFromMonthYear(3, 2021);
 *
 * @param {*} month
 * @param {*} year
 * @param {*} showWeekends
 * @returns
 */
const getDayRangeFromMonthYear = (month, year, showWeekends) => {
  const daysInMonth = new Date(year, month + 1, 0).getDate();
  let dateRange = [];

  for (let i = 1; i <= daysInMonth; i++) {
    let currentDate = new Date(year, month, i);

    // Creates a partial date range array to create weekday only calendars
    if (showWeekends) dateRange = [...dateRange, currentDate];
    else if (currentDate.getDay() !== 0 && currentDate.getDay() !== 6) {
      dateRange = [...dateRange, currentDate];
    }
  }

  return dateRange;
};

module.exports = { getDayRangeFromMonthYear };
