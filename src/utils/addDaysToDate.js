/**
 * Give function a year, month, and day, and how many days you want to increment.
 * Returns new date with added/subtracted days
 *
 * Examples:
 * addDaysToDate(2022, 1, 8, 5) // expect 2022-02-13
 * addDaysToDate(2022, 1, 8, -10) // expect 2022-01-29
 * @param {*} year
 * @param {*} month
 * @param {*} day
 * @param {*} increment
 * @returns
 */

const addDaysToDate = (year, month, day, increment = 0) => {
  let date = new Date(Date.UTC(year, month, day));
  const newDate = new Date(date.getTime() + increment * 86400000);

  return newDate;
};

module.exports = { addDaysToDate };
