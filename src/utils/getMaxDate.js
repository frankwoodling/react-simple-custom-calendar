/**
 * Given an array of date object types return the maximum date.
 * @param {*} dateArray
 * @returns
 */
const getMaxDate = (dateArray) => {
  return new Date(Math.max(...dateArray));
};

module.exports = { getMaxDate };
