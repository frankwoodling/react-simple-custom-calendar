// TODO: Unused code?

export const createFiveDayDateRange = (dateRange) => {
  const fiveDayRange = dateRange.filter((day) =>
    [1, 2, 3, 4, 5].includes(day.getDay())
  );

  return fiveDayRange;
};
