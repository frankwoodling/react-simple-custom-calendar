/**
 * Get the week of the year from the date passed.
 *
 * Source: https://stackoverflow.com/questions/6117814/get-week-of-year-in-javascript-like-in-php
 * Ex: getWeekNumber(2022, 1, 8); // Jan 8, expect 6
 * @param {*} year
 * @param {*} month
 * @param {*} day
 * @returns
 */
function getWeekNumber(year, month, day) {
  let date = new Date(Date.UTC(year, month, day));

  // Set to nearest Thursday: current date + 4 - current day number
  // Make Sunday's day number 7
  date.setUTCDate(date.getUTCDate() + 4 - (date.getUTCDay() || 7));

  // Get first day of year
  var yearStart = new Date(Date.UTC(date.getUTCFullYear(), 0, 1));

  // Calculate full weeks to nearest Thursday
  var weekNumber = Math.ceil(((date - yearStart) / 86400000 + 1) / 7);

  return weekNumber;
}
module.exports = { getWeekNumber };
